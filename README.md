# H4sim
Modification of h4sim used for compelte SM simulation for TB 2018 setup
Work in progress :
- TB_2018 matrix geometry : done
- Beam positon and direction : done
- Table position : done
- TTree output : done
- Root macro to analyze TTree : done
- Position calibration (get Qmax position for each crystal): To be done
- Intercalibration (the angle of crystal is different for each position): To be done
- Linearity (due to different angles of adjacent crystals): To be done

To use it : 
- mkdir TB_2018
- cd TB_2018
- git init
- git remote add origin https://gitlab.cern.ch/dejardin/h4sim.git
- git pull -v origin master
- mkdir ../TB_2018_build
- cd TB_2018_build
- cmake ../TB_2018
- make

Then, have a look at TB_2018/INPUT/test.conf
- ./H4_2018 test.conf

For TB_2018, you don't move the beam, you move the table as in H4 <BR>
/calor/SetPosXY xpos ypos unit (e.g. /calor/SetPosXY 3.4  3.4 cm) <BR>
pos 0. 0. is upper-right corner (with the beam in your back) <BR>
Central crystal a.k.a. C3 should be around 5.6 7.8 cm (calibration in progress)


Warning :

The Geant4 installation is quite tricky on lxplus7 because the data files are not duplicated for all Geant4 versions.
It think that there is certainly a very clever way to set all the environment variables, but I give you my brute-force one :
- source /cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/geant4/10.04-omkpbe3/bin/geant4.sh
- geant4_datadir=/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/
- export G4NEUTRONHPDATA="`cd $geant4_datadir/geant4-G4NDL/4.5/data/G4NDL4.5 > /dev/null ; pwd`"
- export G4LEDATA="`cd $geant4_datadir/geant4-G4EMLOW/7.3/data/G4EMLOW7.3 > /dev/null ; pwd`"
- export G4LEVELGAMMADATA="`cd $geant4_datadir/geant4-G4PhotonEvaporation/5.2/data/PhotonEvaporation5.2 > /dev/null ; pwd`"
- export G4RADIOACTIVEDATA="`cd $geant4_datadir/geant4-G4RadioactiveDecay/5.2/data/RadioactiveDecay5.2 > /dev/null ; pwd`"
- export G4NEUTRONXSDATA="`cd $geant4_datadir/geant4-G4NEUTRONXS/1.4/data/G4NEUTRONXS1.4 > /dev/null ; pwd`"
- export G4SAIDXSDATA="`cd $geant4_datadir/geant4-G4SAIDDATA/1.1/data/G4SAIDDATA1.1 > /dev/null ; pwd`"
- export G4ABLADATA="`cd $geant4_datadir/geant4-G4ABLA/3.0/data/G4ABLA3.0 > /dev/null ; pwd`"
- export G4ENSDFSTATEDATA="`cd $geant4_datadir/geant4-G4ENSDFSTATE/2.2/data/G4ENSDFSTATE2.2 > /dev/null ; pwd`"
- export G4PIIDATA="`cd $geant4_datadir/geant4-/data/G4PII1.3 > /dev/null ; pwd`"
- export G4REALSURFACEDATA="`cd $geant4_datadir/geant4-/data/RealSurface2.1 > /dev/null ; pwd`"

You have also to define H4SIMROOT to run the program:
- export H4SIMROOT=/where_you_have_created_the_directory/TB_2018

Creation : M.D. 2018/05/29 <BR>
Last mod : M.D/ 2018/05/30
