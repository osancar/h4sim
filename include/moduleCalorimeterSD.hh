
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#ifndef moduleCalorimeterSD_h
#define moduleCalorimeterSD_h 1

#include "G4VSensitiveDetector.hh"
#include "globals.hh"

class moduleDetectorConstruction;
class G4HCofThisEvent;
class G4Step;
#include "moduleCalorHit.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class moduleCalorimeterSD : public G4VSensitiveDetector
{
  public:
  
      moduleCalorimeterSD(G4String,moduleDetectorConstruction* det);
     ~moduleCalorimeterSD();

      void Initialize(G4HCofThisEvent*);
      G4bool ProcessHits(G4Step*,G4TouchableHistory*);
      void EndOfEvent(G4HCofThisEvent*);
      void clear();
      void DrawAll();
      void PrintAll();

  private:
  
      moduleCalorHitsCollection*  CalCollection;      
      moduleDetectorConstruction* Detector;
      G4int*                   HitID;
};

#endif

