#ifndef moduleEventAction_h
#define moduleEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "modulePrimaryGeneratorAction.hh"
#include "moduleDetectorConstruction.hh"

#include "TROOT.h"
#include "TFile.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"

class moduleEventActionMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class moduleEventAction : public G4UserEventAction
{
  public:
    moduleEventAction(TTree *ptrOut, moduleDetectorConstruction *detector, modulePrimaryGeneratorAction* mpg);
    virtual ~moduleEventAction();

  public:
    virtual void   BeginOfEventAction(const G4Event*);
    virtual void   EndOfEventAction(const G4Event*);
    
    void SetDrawFlag   (G4String val)  {drawFlag = val;};
    void SetPrintModulo(G4int    val)  {printModulo = val;};
    
  private:
    G4int                         calorimeterCollID;                
    G4String                      drawFlag;
    G4int                         printModulo;                         
    moduleEventActionMessenger*   eventMessenger;
    modulePrimaryGeneratorAction* primaryGeneratorAction;
    moduleDetectorConstruction*   detector;
    TTree*			  ptrtransfert;
    FILE*			  ptrInit_intermediaire;
    
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
