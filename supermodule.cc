#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include "G4UIterminal.hh"
#include "G4UItcsh.hh"

#ifdef G4UI_USE_XM
#include "G4UIXm.hh"
#endif

#include "Randomize.hh"

#ifdef G4UI_USE_XM
#include "moduleVisManager.hh"
#endif

#include "TROOT.h"
#include "TFile.h"
#include "TRandom.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TStopwatch.h"

#include "moduleDetectorConstruction.hh"
#include "modulePhysicsList.hh"
#include "modulePrimaryGeneratorAction.hh"
#include "moduleRunAction.hh"
#include "moduleEventAction.hh"
#include "moduleSteppingAction.hh"
#include "moduleSteppingVerbose.hh"


void usage() {
  cout << "supermodule [config_file]"<< endl;
  exit(-1);
}

int main(int argc,char** argv) 
{
  // *** LLR JG : use $H4SIMROOT environment variable to set further paths
  // *** LLR JG : use G4 variable types for portability

  if (argc>2)
  {
    usage();
  }

  if(getenv("H4SIMROOT") == NULL)
  {
    cout << "No H4SIMROOT defined , Please set environment variable" << endl ;
    exit(0) ;
  }
 
  G4String root_path =  getenv("H4SIMROOT") ;
  cout << "H4SIMROOT : " << root_path  << endl ;

  G4String output ;
  G4String conf_file ;
  G4String input_path ;
  input_path = root_path + "/h4sim/INPUT" ;

  if (argc==1)
  {
    conf_file = input_path + "/test.conf" ;
    cout << "Warning : no config file specified\nDefaulting to " ;
    cout << conf_file << endl;
  }
  else 
  {
    conf_file = input_path + '/' + argv[1] ;
    cout << "config file specified : " << conf_file << endl ;
  }

  printf("Start VIS manager 1\n");
// # ifdef G4VIS_USE
  // visualization manager
  printf("Start VIS manager 2\n");
  G4VisManager* visManager = new moduleVisManager;
  visManager->Initialize();
// # endif
  printf("Start VIS manager 3\n");

  //CONFIG FILE READING

  ifstream conffile;
  conffile.open(conf_file);

  if (!conffile.is_open()) {
    cerr << "argv : " << argv[1] << " File not found " << conf_file << endl;
    exit(-1);
  }
  else 
    cout << "Read config file " << conf_file << endl ;

  G4long seed = 0 ;
  G4String output_path ;
  G4String G4Macro ;
  G4String geometry_path ;
  G4String output_filename ;
  G4String str ;

  while (!conffile.eof()) {
    string line;
    conffile >> line;
    if (line=="#Seed") {
      conffile >> seed;
      cout << "Seed in config file is " << seed << endl;
    } else if (line=="#OutputPath") {
      conffile >> str ;
      output_path = root_path + str ;
      cout << "OutputPath in config file is " << output_path << endl;
    } else if (line=="#GeometryPath") {
      conffile >> str ;
      geometry_path = root_path + str ;
      cout << "GeometryPath in config file is " << geometry_path << endl;
    } else if (line=="#G4Macro") {
      conffile >> str ;
      G4Macro = root_path + str ;
      cout << "G4Macro in config file is " << G4Macro << endl;
    } else if (line=="#OutputFile") {
      conffile >> output_filename;
      cout << "OutputFile in config file is " << output_filename << endl;
    }
  }

  conffile.close();

  //CONFIG FILE READING ENDED

  // ROOT output file
  output = output_path + '/' + output_filename ;
  cout << "Output filename is " << output << endl ;

  TFile *hfile = new TFile(output, "RECREATE","An Example ROOT file");	   
  if (!hfile) {
    cout <<"Cannot Create Output ROOT file. Exiting" << endl;
    exit(-1);
  }
  TTree *myTree = new TTree("monArbre","A ROOT tree");

  // Choose the Random engine
  HepRandom::setTheEngine(new RanecuEngine);
  // Set Seed
  HepRandom::setTheSeed(seed);
  cout << "Random Seed: " << HepRandom::getTheSeed() << endl;

  // Verbose output class
  G4VSteppingVerbose::SetInstance(new moduleSteppingVerbose);

  // Construct the default run manager
  G4RunManager * runManager = new G4RunManager;

  // set mandatory initialization classes
  moduleDetectorConstruction* detector = new moduleDetectorConstruction((char *)geometry_path.c_str());
  runManager->SetUserInitialization(detector);
  runManager->SetUserInitialization(new modulePhysicsList);

  G4UIsession* session=0;
 
  if (argc==1)   // Define UI session for interactive mode.
  {
      // G4UIterminal is a (dumb) terminal.
#ifdef G4UI_USE_XM
      session = new G4UIXm(argc,argv);
#else           
#ifdef G4UI_USE_TCSH
      session = new G4UIterminal(new G4UItcsh);      
#else
      session = new G4UIterminal();
#endif
#endif
  }
  

#ifdef G4UI_USE
      G4UIExecutive * ui = new G4UIExecutive(argc,argv);
      ui->SessionStart();
      delete ui;
#endif
 
    // set user action classes 
  modulePrimaryGeneratorAction* mpg = new modulePrimaryGeneratorAction(detector,(char *)geometry_path.c_str());

  runManager->SetUserAction(mpg);
  runManager->SetUserAction(new moduleRunAction);
  runManager->SetUserAction(new moduleEventAction(myTree,mpg));
  runManager->SetUserAction(new moduleSteppingAction);
    
  //Initialize G4 kernel
  runManager->Initialize(); 

  // get the pointer to the User Interface manager 
  G4UImanager* UI = G4UImanager::GetUIpointer();
  G4String g4cmd = "/control/execute " ;
  UI->ApplyCommand(g4cmd+conf_file);
  // *** LLR JG : Check configuration read from conf_file
  cout << "PrimaryGenerator : " << "Energy " << mpg->GetEnergy() ;
  cout<< " | PosEta "<<mpg->GetposEta() <<",PosPhi "<< mpg->GetposPhi() ;
  cout << ",DTheta " << mpg->GetDTheta() << ",DPhi " << mpg->GetDPhi() << endl ;
  cout<< "BeamXmin "<< mpg->GetBeamXMin()<< ",BeamXmax "<< mpg->GetBeamXMax() ;
  cout<<",BeamXmean "<<mpg->GetBeamXMean()<<",BeamXsigma "<<mpg->GetBeamXSigma();
  cout<< "\nBeamYmin "<< mpg->GetBeamYMin()<<",BeamYmax "<<mpg->GetBeamYMax() ;
  cout<<",BeamYmean "<<mpg->GetBeamYMean()<<",BeamYsigma "<<mpg->GetBeamYSigma();
  cout << endl ;

  if (session)   // Define UI session for interactive mode.
    {
      // G4UIterminal is a (dumb) terminal
#ifdef G4UI_USE_XM
      UI->ApplyCommand("/control/execute vis.mac"); 
      // Customize the G4UIXm menubar with a macro file :
      UI->ApplyCommand("/control/execute  INPUT/gui.mac");
#endif
      cout << " Session has been created " << endl;
      session->SessionStart(); 
      delete session;  
    }
  else           // Batch mode
    { 
      cout << "Batch Mode" << endl;   
      G4String command = "/control/execute ";
      UI->ApplyCommand(command+G4Macro) ;
    }
 cout << "Turning Back" << endl;
 
#ifdef G4UI_USE_XM
 //delete visManager;
#endif

 delete runManager; 
 hfile->Write();
 hfile->Close();
 return 0;
}
