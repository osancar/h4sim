
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "modulePrimaryGeneratorMessenger.hh"

#include "modulePrimaryGeneratorAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

modulePrimaryGeneratorMessenger::modulePrimaryGeneratorMessenger(
                                          modulePrimaryGeneratorAction* moduleGun)
:moduleAction(moduleGun)
{ 
  G4UIdirectory* uidir = new G4UIdirectory("/h4sim/");
  uidir->SetGuidance("h4sim special commands") ;

  beamtype = new G4UIcmdWithAString("/h4sim/beamtype",this);
  beamtype->SetGuidance("Choose beam type");
  beamtype->SetGuidance("  Choice : center(default), tb");
  beamtype->SetParameterName("BeamType",true);
  beamtype->SetDefaultValue("center");
  beamtype->SetCandidates("center testbeam");
  //  beamtype->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  beamprofile = new G4UIcmdWithAString("/h4sim/beamprofile",this);
  beamprofile->SetGuidance("Choose beam profile");
  beamprofile->SetGuidance("  Choice : flat(default), gauss, point");
  beamprofile->SetParameterName("BeamProfile",true);
  beamprofile->SetDefaultValue("flat");
  beamprofile->SetCandidates("flat gauss point");

  posEta = new G4UIcmdWithAnInteger("/h4sim/posEta",this);
  posEta->SetGuidance("Set eta crystal position");
  posEta->SetParameterName("indice_eta",true);
  posEta->SetDefaultValue(1);

  posPhi = new G4UIcmdWithAnInteger("/h4sim/posPhi",this);
  posPhi->SetGuidance("Set phi crystal position");
  posPhi->SetParameterName("indice_phi",true);
  posPhi->SetDefaultValue(1);

  beamxmin = new G4UIcmdWithADoubleAndUnit("/h4sim/beamxmin",this);
  beamxmin->SetGuidance("Set Beam X min (flat beam profile)");
  beamxmin->SetParameterName("beamxmin",true,true);
  beamxmin->SetDefaultUnit("mm");
  beamxmin->SetUnitCandidates("micron mm cm m km");

  beamxmax =  new G4UIcmdWithADoubleAndUnit("/h4sim/beamxmax",this);
  beamxmax->SetGuidance("Set Beam X max (flat beam profile)");
  beamxmax->SetParameterName("beamxmax",true,true);
  beamxmax->SetDefaultUnit("mm");
  beamxmax->SetUnitCandidates("micron mm cm m km");

  beamxmean =  new G4UIcmdWithADoubleAndUnit("/h4sim/beamxmean",this);
  beamxmean->SetGuidance("Set Beam X Mean (gaussian beam profile)");
  beamxmean->SetParameterName("beamxmean",true,true);
  beamxmean->SetDefaultUnit("mm");
  beamxmean->SetUnitCandidates("micron mm cm m km");

  beamxsigma =  new G4UIcmdWithADoubleAndUnit("/h4sim/beamxsigma",this);
  beamxsigma->SetGuidance("Set Beam X Sigma (gaussian beam profile)");
  beamxsigma->SetParameterName("beamxsigma",true,true);
  beamxsigma->SetDefaultUnit("mm");
  beamxsigma->SetUnitCandidates("micron mm cm m km");

  beamymin =  new G4UIcmdWithADoubleAndUnit("/h4sim/beamymin",this);
  beamymin->SetGuidance("Set Beam Y min (flat beam profile)");
  beamymin->SetParameterName("beamymin",true,true);
  beamymin->SetDefaultUnit("mm");
  beamymin->SetUnitCandidates("micron mm cm m km");

  beamymax =  new G4UIcmdWithADoubleAndUnit("/h4sim/beamymax",this);
  beamymax->SetGuidance("Set Beam Y max (flat beam profile)");
  beamymax->SetParameterName("beamymax",true,true);
  beamymax->SetDefaultUnit("mm");
  beamymax->SetUnitCandidates("micron mm cm m km");

  beamymean =  new G4UIcmdWithADoubleAndUnit("/h4sim/beamymean",this);
  beamymean->SetGuidance("Set Beam Y Mean (gaussian beam profile)");
  beamymean->SetParameterName("beamymean",true,true);
  beamymean->SetDefaultUnit("mm");
  beamymean->SetUnitCandidates("micron mm cm m km");

  beamysigma =  new G4UIcmdWithADoubleAndUnit("/h4sim/beamysigma",this);
  beamysigma->SetGuidance("Set Beam Y Sigma (gaussian beam profile)");
  beamysigma->SetParameterName("beamysigma",true,true);
  beamysigma->SetDefaultUnit("mm");
  beamysigma->SetUnitCandidates("micron mm cm m km");

  dx = new G4UIcmdWithADoubleAndUnit("/h4sim/dx",this);
  dx->SetGuidance("Set Dx on the front face of the xtal");
  dx->SetParameterName("dx",true,true);
  dx->SetDefaultUnit("mm");
  dx->SetUnitCandidates("micron mm cm m km");

  dy = new G4UIcmdWithADoubleAndUnit("/h4sim/dy",this);
  dy->SetGuidance("Set Dy on the front face of the xtal");
  dy->SetParameterName("dy",true,true);
  dy->SetDefaultUnit("mm");
  dy->SetUnitCandidates("micron mm cm m km");

  dtheta = new G4UIcmdWithADouble("/h4sim/dtheta",this);
  dtheta->SetGuidance("Set Dtheta of the beam");
  dtheta->SetParameterName("dtheta",true,true);

  dphi = new G4UIcmdWithADouble("/h4sim/dphi",this);
  dphi->SetGuidance("Set Dphi of the beam");
  dphi->SetParameterName("dphi",true,true);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

modulePrimaryGeneratorMessenger::~modulePrimaryGeneratorMessenger()
{
  //  delete RndmCmd;

  delete beamtype;
  delete beamprofile;
  delete posEta;
  delete posPhi;
  delete beamxmin;
  delete beamxmax;
  delete beamxmean;
  delete beamxsigma;
  delete beamymin;
  delete beamymax;
  delete beamymean;
  delete beamysigma;
  delete dx;
  delete dy;
  delete dtheta;
  delete dphi ;

  //  delete Balai_Cmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void modulePrimaryGeneratorMessenger::SetNewValue(
                                        G4UIcommand* command, G4String newValue)
{
  /*
  if( command == RndmCmd )
   { moduleAction->SetRndmFlag(newValue);}
  */

  if( command == beamtype )
   { moduleAction->SetBeamType(newValue);}
  if( command == beamprofile )
   { moduleAction->SetBeamProfile(newValue);}
  if( command == posPhi )
    { moduleAction->SetposPhi(posPhi->GetNewIntValue(newValue));}
  if( command == posEta )
    { moduleAction->SetposEta(posEta->GetNewIntValue(newValue));}
//   if( command == xoff )
//    { moduleAction->SetXOff(xoff->GetNewDoubleValue(newValue));}
//   if( command == yoff )
//    { moduleAction->SetYOff(yoff->GetNewDoubleValue(newValue));}
  if( command == dtheta )
   { moduleAction->SetDTheta(dtheta->GetNewDoubleValue(newValue));}
  if( command == dphi )
   { moduleAction->SetDPhi(dphi->GetNewDoubleValue(newValue));}
  if( command == dx )
   { moduleAction->SetDeltaX(dx->GetNewDoubleValue(newValue));}
  if( command == dy )
   { moduleAction->SetDeltaY(dy->GetNewDoubleValue(newValue));}
  if( command == beamxmin )
   { moduleAction->SetBeamXMin(beamxmin->GetNewDoubleValue(newValue));}
  if( command == beamxmax )
   { moduleAction->SetBeamXMax(beamxmax->GetNewDoubleValue(newValue));}
  if( command == beamxmean )
   { moduleAction->SetBeamXMean(beamxmean->GetNewDoubleValue(newValue));}
  if( command == beamxsigma )
   { moduleAction->SetBeamXSigma(beamxsigma->GetNewDoubleValue(newValue));}
  if( command == beamymin )
   { moduleAction->SetBeamYMin(beamymin->GetNewDoubleValue(newValue));}
  if( command == beamymax )
   { moduleAction->SetBeamYMax(beamymax->GetNewDoubleValue(newValue));}
  if( command == beamymean )
   { moduleAction->SetBeamYMean(beamymean->GetNewDoubleValue(newValue));}
  if( command == beamysigma )
   { moduleAction->SetBeamYSigma(beamysigma->GetNewDoubleValue(newValue));}


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

