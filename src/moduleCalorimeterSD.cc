
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "moduleCalorimeterSD.hh"

#include "moduleCalorHit.hh"
#include "moduleDetectorConstruction.hh"

#include "G4VPhysicalVolume.hh"
#include "G4Step.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4SDManager.hh"

#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleCalorimeterSD::moduleCalorimeterSD(G4String name,
          moduleDetectorConstruction* det)
:G4VSensitiveDetector(name),Detector(det)
{
  collectionName.insert("CalCollection");
  HitID = new G4int[1];
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleCalorimeterSD::~moduleCalorimeterSD()
{
  delete [] HitID;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleCalorimeterSD::Initialize(G4HCofThisEvent *HCE)
{
  CalCollection = new moduleCalorHitsCollection
                      (SensitiveDetectorName,collectionName[0]);
  HitID[0] = -1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool moduleCalorimeterSD::ProcessHits(G4Step* aStep,G4TouchableHistory* ROhist)
{
  int i = 0;
  int test = 0;
  G4double edep = aStep->GetTotalEnergyDeposit();
  
  G4double stepl = 0.;
  if (aStep->GetTrack()->GetDefinition()->GetPDGCharge() != 0.)
      stepl = aStep->GetStepLength();
      
  if ((edep==0.)&&(stepl==0.)) return false;      

  G4TouchableHistory* theTouchable = (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable());
    
  G4VPhysicalVolume* physVol = theTouchable->GetVolume(); 
  //theTouchable->MoveUpHistory();
 
  if (HitID[0]==-1)
  { 
    moduleCalorHit* calHit = new moduleCalorHit();    
    for(i=0;i<nb_crystal;i++)
    {
      if(physVol == Detector->GetCristal(i)) test = i;
    }
    calHit->AddAbs(edep,stepl,test);    
    HitID[0] = CalCollection->insert(calHit) - 1;
    if (verboseLevel>0)
    G4cout << " New Calorimeter Hit on layer: " <<  G4endl;
  }
  else
  { 
    for(i=0;i<nb_crystal;i++)
    {
        if(physVol == Detector->GetCristal(i)) test = i;
    
    }
      (*CalCollection)[HitID[0]]->AddAbs(edep,stepl,test);
      if (verboseLevel>0)
      G4cout << " Energy added to Layer: " <<  G4endl; 
  }
    
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleCalorimeterSD::EndOfEvent(G4HCofThisEvent* HCE)
{
  static G4int HCID = -1;
  if(HCID<0)
  { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  HCE->AddHitsCollection(HCID,CalCollection);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleCalorimeterSD::clear()
{} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleCalorimeterSD::DrawAll()
{} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleCalorimeterSD::PrintAll()
{
G4cout<<this->active<<G4endl;
G4cout<<this->SensitiveDetectorName<<G4endl;
} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

