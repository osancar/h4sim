#include "moduleCalorHit.hh"

G4Allocator<moduleCalorHit> moduleCalorHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleCalorHit::moduleCalorHit()
{
  for (int i=0;i<nb_crystal;i++)
  {
    EdepAbs[i] = 0.; TrackLengthAbs[i] = 0.;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleCalorHit::~moduleCalorHit()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleCalorHit::moduleCalorHit(const moduleCalorHit& right)
{
  for (int i=0;i<nb_crystal;i++)
  {
    EdepAbs[i] = right.EdepAbs[i]; TrackLengthAbs[i] = right.TrackLengthAbs[i];
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const moduleCalorHit& moduleCalorHit::operator=(const moduleCalorHit& right)
{
  for (int i=0;i<nb_crystal;i++)
  {
    EdepAbs[i] = right.EdepAbs[i]; TrackLengthAbs[i] = right.TrackLengthAbs[i];
  }
  
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int moduleCalorHit::operator==(const moduleCalorHit& right) const
{
  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleCalorHit::Draw()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleCalorHit::Print()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

