
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "moduleSteppingVerbose.hh"

#include "G4SteppingManager.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleSteppingVerbose::moduleSteppingVerbose()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

moduleSteppingVerbose::~moduleSteppingVerbose()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

using namespace std;
void moduleSteppingVerbose::StepInfo()
{
  CopyState();
  
  G4int prec = G4cout.precision(3);

  if( verboseLevel >= 1 ){
    if( verboseLevel >= 4 ) VerboseTrack();
    if( verboseLevel >= 3 ){
      G4cout << G4endl;    
      G4cout << setw( 5) << "#Step#"     << " "
	     << setw( 6) << "X"          << "    "
	     << setw( 6) << "Y"          << "    "  
	     << setw( 6) << "Z"          << "    "
	     << setw( 9) << "KineE"      << " "
	     << setw( 9) << "dEStep"     << " "  
	     << setw(10) << "StepLeng"     
	     << setw(10) << "TrakLeng" 
	     << setw(10) << "Volume"    << "  "
	     << setw(10) << "Process"   << G4endl;	          
    }

    G4cout << setw(5) << fTrack->GetCurrentStepNumber() << " "
    << setw(6) << G4BestUnit(fTrack->GetPosition().x(),"Length")
    << setw(6) << G4BestUnit(fTrack->GetPosition().y(),"Length")
    << setw(6) << G4BestUnit(fTrack->GetPosition().z(),"Length")
    << setw(6) << G4BestUnit(fTrack->GetKineticEnergy(),"Energy")
    << setw(6) << G4BestUnit(fStep->GetTotalEnergyDeposit(),"Energy")
    << setw(6) << G4BestUnit(fStep->GetStepLength(),"Length")
    << setw(6) << G4BestUnit(fTrack->GetTrackLength(),"Length");

    // if( fStepStatus != fWorldBoundary){ 
    if( fTrack->GetNextVolume() != 0 ) { 
      G4cout << setw(10) << fTrack->GetVolume()->GetName();
    } else {
      G4cout << setw(10) << "OutOfWorld";
    }

    if(fStep->GetPostStepPoint()->GetProcessDefinedStep() != 0){
      G4cout << "  "
             << setw(10)
	     << fStep->GetPostStepPoint()->GetProcessDefinedStep()
	                                 ->GetProcessName();
    } else {
      G4cout << "   UserLimit";
    }

    G4cout << G4endl;

    if( verboseLevel == 2 ){
      G4int tN2ndariesTot = fN2ndariesAtRestDoIt +
	                    fN2ndariesAlongStepDoIt +
	                    fN2ndariesPostStepDoIt;
      if(tN2ndariesTot>0){
	G4cout << "    :----- List of 2ndaries - "
	       << "#SpawnInStep=" << setw(3) << tN2ndariesTot 
	       << "(Rest="  << setw(2) << fN2ndariesAtRestDoIt
	       << ",Along=" << setw(2) << fN2ndariesAlongStepDoIt
	       << ",Post="  << setw(2) << fN2ndariesPostStepDoIt
	       << "), "
	       << "#SpawnTotal=" << setw(3) << (*fSecondary).size()
	       << " ---------------"
	       << G4endl;

	for(size_t lp1=(*fSecondary).size()-tN2ndariesTot; 
                        lp1<(*fSecondary).size(); lp1++){
	  G4cout << "    : "
		 << setw(6)
		 << G4BestUnit((*fSecondary)[lp1]->GetPosition().x(),"Length")
		 << setw(6)
		 << G4BestUnit((*fSecondary)[lp1]->GetPosition().y(),"Length")
		 << setw(6)
		 << G4BestUnit((*fSecondary)[lp1]->GetPosition().z(),"Length")
		 << setw(6)
		 << G4BestUnit((*fSecondary)[lp1]->GetKineticEnergy(),"Energy")
		 << setw(10)
		 << (*fSecondary)[lp1]->GetDefinition()->GetParticleName();
	  G4cout << G4endl;
	}
              
	G4cout << "    :-----------------------------"
	       << "----------------------------------"
	       << "-- EndOf2ndaries Info ---------------"
	       << G4endl;
      }
    }
    
  }
  G4cout.precision(prec);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void moduleSteppingVerbose::TrackingStarted()
{

  CopyState();
G4int prec = G4cout.precision(3);
  if( verboseLevel > 0 ){

    G4cout << setw( 5) << "Step#"      << " "
           << setw( 6) << "X"          << "    "
	   << setw( 6) << "Y"          << "    "  
	   << setw( 6) << "Z"          << "    "
	   << setw( 9) << "KineE"      << " "
	   << setw( 9) << "dEStep"     << " "  
	   << setw(10) << "StepLeng"  
	   << setw(10) << "TrakLeng"
	   << setw(10) << "Volume"     << "  "
	   << setw(10) << "Process"    << G4endl;	     

   G4cout << setw( 5) << fTrack->GetCurrentStepNumber() << " "
    << setw( 6) << G4BestUnit(fTrack->GetPosition().x(),"Length")
    << setw( 6) << G4BestUnit(fTrack->GetPosition().y(),"Length")
    << setw( 6) << G4BestUnit(fTrack->GetPosition().z(),"Length")
    << setw( 6) << G4BestUnit(fTrack->GetKineticEnergy(),"Energy")
    << setw( 6) << G4BestUnit(fStep->GetTotalEnergyDeposit(),"Energy")
    << setw( 6) << G4BestUnit(fStep->GetStepLength(),"Length")
    << setw( 6) << G4BestUnit(fTrack->GetTrackLength(),"Length");

    if(fTrack->GetNextVolume()){
      G4cout << setw(10) << fTrack->GetVolume()->GetName();
    } else {
      G4cout << "OutOfWorld";
    }
    G4cout << "    initStep" << G4endl;
  }
  G4cout.precision(prec);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
